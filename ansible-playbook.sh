#!/bin/bash

set -o nounset -o pipefail -o errexit

# Load variables from .env
set -o allexport
source "$(dirname "$0")/.env"
set +o allexport

# Run ansible
exec ansible-playbook "$@"
