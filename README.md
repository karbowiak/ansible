# Remember to add the following to the ~/.ssh/config
```
Host contabo1
  HostName <ip>
  Port 22
  IdentityFile ~/.ssh/ansible_rsa
  User root

Host contabo2
  HostName <ip>
  Port 22
  IdentityFile ~/.ssh/ansible_rsa
  User root
```

# Galaxies to install
1. ansible-galaxy collection install community.general
2. ansible-galaxy collection install kubernetes.core

# How to run
1. Add your servers to your ~/.ssh/config as written above (feel free to change Host names)
2. Generate your ssh key with `ssh-keygen`
3. Copy your public key to the servers you want to deploy to (in the ~/.ssh/authorized_keys file for the root user)
4. Edit hosts.yml and insert your own servers
5. Run it with `./ansible-playbook.sh -i hosts.yml site.yml`

# Caveats
It is only tested with servers running Ubuntu 20.04 LTS.
